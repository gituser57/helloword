﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWord
{
    public delegate void Del(string str);

    class Program
    {
        public static void WriteToConsole(string str)
        {
            Console.WriteLine(str);
        }

        public static void DoubleWriteToConsole(string str)
        {
            Console.WriteLine(str + str);
        }

        public static void WrongWriteToConsole()
        {
            Console.WriteLine("   ");
        }
        static void Main(string[] args)
        {

            //WriteToConsole("Hello, World");

            Action<string> del = WriteToConsole;
            del += DoubleWriteToConsole;
            //del += WrongWriteToConsole;

            del?.Invoke("Hello, World");

            del -= DoubleWriteToConsole;
            del -= WriteToConsole;
            // del += WriteToConsole;gybyh
            del?.Invoke("Hello, World");

            Func<string, int> del2 = s => s.Length;
            int len;
            if (del2 != null)
                len = del2("zachem gerasim utopil mumu?");

            IEnumerable<string> list = new string[] { "mama", "mila-mila", "ramu" };
            IEnumerable<int> list2 = list.Select(s => s.Length).ToList();
        }
    }
}
